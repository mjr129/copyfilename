# CopyFilename #

A quick and simple application that allows you to quickly **copy filenames from the Explorer context menu** in a variety of formats.

This is a **very simple stand alone application** that doesn't install any context handlers or make any system changes, but you will need to add the menu option yourself (see below!).

## Getting started from source ##

Compile using Visual Studio.
Nothing esoteric required.

## Getting started from binary ##

The application takes one argument as a parameter, the full path to the filename to copy.
Holding ctrl presents a list of formats (e.g. backslash escaped for C++, using forward slashes for R, etc.)

## Adding menu options (if you didn't already know) ##
Later versions of Windows don't have a nice UI for this, but as an example to add a "Copy filename..." menu item for ALL FILE TYPES you would add the following registry key:

    Windows Registry Editor Version 5.00
    
    [HKEY_CLASSES_ROOT\*\shell\Copy Filename\command]
    @="C:\\Program Files\\CopyToClipboard\\CopyToClipboard.exe \"%1\""

## Screenshots ##
![Screenshots.png](https://bitbucket.org/repo/pdrXr7/images/3167854174-Screenshots.png)