﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CopyToClipboard
{
    public partial class FrmChoices : Form
    {
        public static string Show(string[] list)
        {
            using (var frm = new FrmChoices())
            {
                frm.Left = System.Windows.Forms.Cursor.Position.X - frm.Width / 2;
                frm.Top = System.Windows.Forms.Cursor.Position.Y + frm.Height / 2;

                foreach (var item in list)
                {
                    frm.listBox1.Items.Add(item);
                }

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    return (string)frm.listBox1.SelectedItem;
                }

                return null;
            }
        }

        public FrmChoices()
        {
            InitializeComponent();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
