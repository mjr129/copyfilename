﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CopyToClipboard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string[] args = Environment.GetCommandLineArgs();

            if (args.Length >= 2)
            {
                string file = args[1];

                if ((ModifierKeys & Keys.Control) == Keys.Control)
                {
                    List<string> opts = new List<string>();

                    opts.Add(file);
                    opts.Add(file.Replace("\\", "\\\\"));
                    opts.Add(file.Replace("\\", "/"));
                    opts.Add(Path.GetFileName(file));
                    opts.Add(Path.GetDirectoryName(file));

                    file = FrmChoices.Show(opts.ToArray());
                }

                if (file != null)
                {
                    Clipboard.SetText(file);
                    label2.Text = file;
                }
                else
                {
                    label2.Text = "(cancelled)";
                }
            }
            else
            {
                label2.Text = "(no text)";
            }

            this.Left = System.Windows.Forms.Cursor.Position.X - this.Width / 2;
            this.Top = System.Windows.Forms.Cursor.Position.Y + this.Height / 2;
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
